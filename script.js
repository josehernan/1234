if ('serviceWorker' in navigator) {
	window.addEventListener('load', function () {
		navigator.serviceWorker.register('/service-worker.js')
			.then( function (reg) {
				console.log('Service worker registered.', reg);
			})
	})
}


let deferredPrompt;
const addBtn = document.querySelector('.add-button');
addBtn.style.display = 'none';

window.addEventListener('beforeinstallprompt', function (e) {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI to notify the user they can add to home screen
  addBtn.style.display = 'block';

  addBtn.addEventListener('click', function (e) {
    // hide our user interface that shows our A2HS button
    addBtn.style.display = 'none';
    // Show the prompt
    deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice.then( function (choiceResult) {
        if (choiceResult.outcome === 'accepted') {
          console.log('User accepted the A2HS prompt');
        } else {
          console.log('User dismissed the A2HS prompt');
        }
        deferredPrompt = null;
      });
  });
});

function horaActual () {
	var d = new Date()
	var s = d.getHours() + '' + d.getMinutes()
	return s
}

function es1234() {
	if(horaActual() === '1234') { return true }
	else { return false }
}

function consultarHora() {
	var p = document.querySelector('#respuesta')
	if( es1234() ) { p.textContent = 'SI' }
	else { p.textContent = 'NO' }
}

setInterval( function (){ consultarHora() }, 5000);

consultarHora()

